The simplest cluster implementation I could come up with
--------------------------------------------------------

The key to configure the cluster is the application.conf resource.
It defines two seed nodes, on ports 10001 and 10002 on localhost.
Seed nodes start even if they are the only node in the cluster.
Every other node will try to connect to those nodes on startup and
will not start unless connected to at least one.

To start one of the two seed nodes:
 - sbt seed1
 - sbt seed2

To start a cluster node with no actors:
 - sbt empty

To start a cluster node with a listener actor:
 - sbt listener

Press CTRL-C to stop a node.
