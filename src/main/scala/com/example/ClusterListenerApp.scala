package com.example

import com.typesafe.config.ConfigFactory

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorSystem
import akka.actor.Props
import akka.cluster.Cluster
import akka.cluster.ClusterEvent._

object ClusterListenerApp extends App {

  val config = ConfigFactory.load()
  val system = ActorSystem(MyCluster.name, config)

  system.actorOf(Props[SimpleClusterListener], name = "cluster-listener")
}

class SimpleClusterListener extends Actor {

  val cluster = Cluster(context.system)

  override def preStart(): Unit = {
    cluster.subscribe(self, initialStateMode = InitialStateAsEvents,
      classOf[MemberEvent], classOf[UnreachableMember])
  }

  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive = {
    case MemberUp(member) =>
      println(s"Member is Up: ${member.address}")
    case UnreachableMember(member) =>
      println(s"Member detected as unreachable: ${member.address}")
    case MemberRemoved(member, previousStatus) =>
      println(s"Member is Removed: ${member.address}")
    case _: MemberEvent => // ignore
  }
}
