package com.example

import akka.actor.ActorSystem
import akka.cluster.Cluster
import com.typesafe.config.ConfigFactory
import akka.actor.Props
import akka.actor.Props

object EmptyClusterApp extends App {

  val port = if (args.isEmpty) 0 else args(0)

  val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=${port}").
        withFallback(ConfigFactory.load())

  val system = ActorSystem(MyCluster.name, config)
}