name := """akka-cluster-sample"""

scalaVersion := "2.11.1"

val akkaVersion = "2.3.4"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster" % akkaVersion
)

addCommandAlias("seed1", "runMain com.example.EmptyClusterApp 10001")

addCommandAlias("seed2", "runMain com.example.EmptyClusterApp 10002")

addCommandAlias("empty", "runMain com.example.EmptyClusterApp")

addCommandAlias("listener", "runMain com.example.ClusterListenerApp")

